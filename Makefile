# The entire webdoc source tree must be located under the doc subdirectory
# of the Eclat tree.

include ../Makefile.inc
VPATH=..

.SUFFIXES: .1 .5 .htin .html

MAN2HTMLFLAGS=-r

.1.html:
	man2html $(MAN2HTMLFLAGS) $< | \
	 sed 's/<[\/A-Z][A-Z0-9]*/\L&/g;s|<b>\\dq</b>|<b>\\"</b>|g;s|<b>dq</b>|<b>"</b>|g' | \
         awk -f postproc.awk
	m4 -P -DACTIVE=documentation inc/header.html a.1 a.3 a.2 a.4 inc/footer.html > $@
	rm a.[1-4]
.5.html:
	man2html $(MAN2HTMLFLAGS) $< | \
	 sed 's/<[\/A-Z][A-Z0-9]*/\L&/g' | \
         awk -f postproc.awk
	m4 -P -DACTIVE=documentation inc/header.html a.1 a.3 a.2 a.4 inc/footer.html > $@
	rm a.[1-4]

.htin.html:
	m4 -P -DACTIVE=$* inc/header.html $< inc/footer.html > $@

SOURCES=\
 index.htin\
 download.htin\
 examples.htin

HTMLFILES=$(MANPAGES1:.1=.html) $(MANPAGES5:.5=.html) $(SOURCES:.htin=.html)

all: $(HTMLFILES)

archive: all
	tar zcf eclat-webdoc.tar.gz $(HTMLFILES) *.css graphics 

clean:; rm -f *.html a.[1-4] *~

$(HTMLFILES): Makefile postproc.awk inc/header.html inc/footer.html 

